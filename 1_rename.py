#!/usr/bin/python

# Each picture taken with the microscope is a frame. We want to compose 
# several frames into a larger image. We use a "matrix" convention for 
# all the coordinates systems we use: the origin (0,0) is in the upper 
# left corner; the first coordinate is called 'i' and indicates the row; 
# the second coordinate is called 'j' and indicates the column; i varies
# from 0 to n, j varies from 0 to m. Whenever a 2-dimensional matrix is
# represented as a 1-dimensional vector, we use the C array convention
#   1 2 3
#   4 5 6   <-->  1 2 3 4 5 6 7 8 9
#   7 8 9
#
# The microscope software numbers the frames it acquires in ascending order 
# starting from 001. If the larger image we want to assemble is made of 10x10 
# frames, this is how the frames should be composed:
#
# 001 002 ... 010
# 011 011 ... 020   This is the larger image at time 0
# ... ... ... ...
# 091 092 ... 100
#
# 101 102 ... 110
# 111 111 ... 120   This is the large image at time 1
# ... ... ... ...
# 191 192 ... 200
#
# We often acquire multiple frames for the same spot (e.g. bright field, yfp 
# and cfp fluorescence). Since the microscope software still numbers the 
# frames in ascending order, we compose the larger images as follows:
#
# 001 004 ... 028
# 031 034 ... 058   This is the larger bright field image at time 0
# ... ... ... ...
# 271 274 ... 298
#
# 002 005 ... 029
# 032 035 ... 059   This is the large yfp fluorescence image at time 0
# ... ... ... ...
# 272 275 ... 299
#
# 003 006 ... 030
# 033 036 ... 060   This is the larger cfp fluorescence image at time 0
# ... ... ... ...
# 273 276 ... 300
#
# 301 304 ... 328
# 331 334 ... 358   This is the larger bright field image at time 1
# ... ... ... ...
# 571 574 ... 598
#
# The standard order when multiple 'colors' are acquired for the same spot is:
# bright field, and then in order of decreasing excitation/emission wavelength
# e.g.: brigth field, rfp, yfp, gfp, cfp.
#
#
# We rename the output frames from the microscope software with the format:
# time_i_j_color. For instance, for a large image of size 10x10, yfp and cfp
# acquisition, we would have:
#
# 001.tif -> 000_000_000_yfp 
# 002.tif -> 000_000_000_cfp
# 003.tif -> 000_000_001_yfp 
# 004.tif -> 000_000_001_cfp
# ...
# 019.tif -> 000_000_009_yfp
# 020.tif -> 000_000_009_cfp
# 021.tif -> 000_001_000_yfp
# 022.tif -> 000_001_000_cfp
# ...
# 199.tif -> 000_009_009_yfp
# 200.tif -> 000_009_009_cfp
# 201.tif -> 001_000_000_yfp
# 202.tif -> 001_000_000_cfp
# ...
#

import os

dirname = '/media/My Passport/Swimmers/20130616/RawData16Bits/'
n, m = 13, 13
colors = ['bf', 'yfp', 'cfp']

n_colors = len(colors)
for from_file in os.listdir(dirname):
    old_file_number = int(from_file.split('.')[0])
    t = (old_file_number-1) / (n * m * n_colors)
    i = ((old_file_number-1) % (n * m * n_colors)) / (m * n_colors)
    j = ((old_file_number-1) % (m * n_colors)) / n_colors
    c = (old_file_number-1) % n_colors
    
    t = str(t).zfill(3)
    i = str(i).zfill(3)
    j = str(j).zfill(3)
    to_file = t + '_' + i + '_' + j + '_' + colors[c]
    
    print 'Renaming', from_file, 'to', to_file
    os.rename(dirname + from_file, dirname + to_file)
